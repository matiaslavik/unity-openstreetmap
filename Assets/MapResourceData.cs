﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapResourceData", menuName = "ScriptableObjects/MapResourceData", order = 1)]
public class MapResourceData : ScriptableObject
{
    public Material roadMaterial;
    public Material buildingMaterial;

    private static MapResourceData _instance;

    public static MapResourceData instance
    {
        get
        {
            if (_instance == null)
                _instance = Resources.Load<MapResourceData>("MapResourceData");
            return _instance;
        }
    }
}
