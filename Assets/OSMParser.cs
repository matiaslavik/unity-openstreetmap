﻿using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class OSMParser
{
    private Dictionary<long, NodeInfo> nodes = new Dictionary<long, NodeInfo>();
    private Dictionary<long, WayNodeInfo> wayNodes = new Dictionary<long, WayNodeInfo>();
    private Dictionary<long, WayInfo> ways = new Dictionary<long, WayInfo>();

    private Dictionary<string, RoadType> roadTypesByName = new Dictionary<string, RoadType>() {
        { "motorway", RoadType.Motorway}, { "trunk", RoadType.Trunk }, { "primary", RoadType.Primary }, { "secondary", RoadType.Secondary },
        { "tertiary", RoadType.Tertiary}, { "unclassified", RoadType.Unclassified }, { "residential", RoadType.Residential }, { "link", RoadType.Link }};

    public MapAreaData ParseMap(string filepath)
    {
        System.Xml.XmlDocument doc = new XmlDocument();
        doc.Load(filepath);
        
        // Parse nodes
        var nodeElements = doc.GetElementsByTagName("node");
        foreach (XmlNode nodeElem in nodeElements)
        {
            NodeInfo node = new NodeInfo();
            node.id = long.Parse(nodeElem.Attributes["id"].InnerText);
            node.lat = double.Parse(nodeElem.Attributes["lat"].InnerText);
            node.lon = double.Parse(nodeElem.Attributes["lon"].InnerText);
            nodes.Add(node.id, node);
        }

        // Parse ways
        var wayElements = doc.GetElementsByTagName("way");
        foreach (XmlNode wayElem in wayElements)
        {
            WayInfo way = new WayInfo();
            way.id = long.Parse(wayElem.Attributes["id"].InnerText);
            way.wayNodes = new List<WayNodeInfo>();

            // Parse way nodes
            foreach (XmlNode wayNodeElem in wayElem.ChildNodes)
            {
                if (wayNodeElem.Attributes[0].Name == "ref")
                {
                    long wayNodeID = long.Parse(wayNodeElem.Attributes["ref"].InnerText);
                    WayNodeInfo wayNode = null;
                    if (!wayNodes.TryGetValue(wayNodeID, out wayNode))
                    {
                        wayNode = new WayNodeInfo();
                        wayNode.node = nodes[wayNodeID];
                        wayNode.connectedWays = new List<WayInfo>();
                        wayNodes.Add(wayNodeID, wayNode);
                    }
                    wayNode.connectedWays.Add(way);
                    way.wayNodes.Add(wayNode);
                }
                else if (wayNodeElem.Attributes[0].Name == "k")
                {
                    if (wayNodeElem.Attributes[0].InnerText == "highway")
                    {
                        string highwayType = wayNodeElem.Attributes[1].InnerText;
                        RoadType roadType;
                        if (roadTypesByName.TryGetValue(highwayType, out roadType))
                        {
                            way.roadInfo = new RoadInfo();
                            way.roadInfo.roadType = roadType;
                        }
                    }
                    else if (wayNodeElem.Attributes[0].InnerText == "maxspeed" && way.roadInfo != null)
                    {
                        way.roadInfo.speedLimit = Convert.ToInt32(wayNodeElem.Attributes[1].InnerText);
                    }
                    else if (wayNodeElem.Attributes[0].InnerText == "building")
                    {
                        way.buildingInfo = new BuildingInfo();
                    }
                }
            }

            ways.Add(way.id, way);
        }

        Debug.Log(ways.Count);
        Debug.Log(wayNodes.Count);

        MapAreaData mapData = new MapAreaData();
        mapData.nodes = nodes;
        mapData.ways = ways;
        return mapData;
    }
}
