﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum RoadType
{
    Motorway,
    Trunk,
    Primary,
    Secondary,
    Tertiary,
    Unclassified,
    Residential,
    Link
}

public class NodeInfo
{
    public long id;
    public double lat;
    public double lon;
}

public class WayNodeInfo
{
    public NodeInfo node;
    public List<WayInfo> connectedWays;
}

public class WayInfo
{
    public long id;
    public List<WayNodeInfo> wayNodes;
    public RoadInfo roadInfo = null;
    public BuildingInfo buildingInfo = null;
}

public class RoadInfo
{
    public RoadType roadType;
    public int speedLimit;
}

public class BuildingInfo
{
    public int floors = 1;
}
