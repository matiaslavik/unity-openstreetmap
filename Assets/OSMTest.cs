﻿using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class OSMTest : MonoBehaviour
{
    private Dictionary<long, NodeInfo> nodes = new Dictionary<long, NodeInfo>();
    private Dictionary<long, WayNodeInfo> wayNodes = new Dictionary<long, WayNodeInfo>();
    private Dictionary<long, WayInfo> ways = new Dictionary<long, WayInfo>();

    private HashSet<long> processedRoads = new HashSet<long>();

    private Dictionary<string, RoadType> roadTypesByName = new Dictionary<string, RoadType> () {
        { "motorway", RoadType.Motorway}, { "trunk", RoadType.Trunk }, { "primary", RoadType.Primary }, { "secondary", RoadType.Secondary },
        { "tertiary", RoadType.Tertiary}, { "unclassified", RoadType.Unclassified }, { "residential", RoadType.Residential }, { "link", RoadType.Link }};

    void Start()
    {
        string filepath = Path.Combine(Application.dataPath, "Resources", "map2.osm");
        OSMParser parser = new OSMParser();
        MapAreaData mapData = parser.ParseMap(filepath);

        MapConstructor mapConstructor = new MapConstructor();
        mapConstructor.ConstructMap(mapData);
    }

    void Update()
    {
        
    }
}
