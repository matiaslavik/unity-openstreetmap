﻿using System;
using System.Collections.Generic;

public class MapAreaData
{
    public Dictionary<long, NodeInfo> nodes;
    public Dictionary<long, WayInfo> ways;
}
