﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RoadMeshConstructor
{
    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();
    private List<Vector2> uv = new List<Vector2>();
    private List<int> indices = new List<int>();

    private Vector3 prevPoint;
    private int numPoints;
    private float currTexV;

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = indices.ToArray();
        mesh.uv = uv.ToArray();
        return mesh;
    }

    public void AddPoint(Vector3 point, float width)
    {
        numPoints++;

        float segmentLength = width * 2.0f;

        if (numPoints > 1)
        {
            Vector3 dir = point - prevPoint;
            Vector3 forward = Vector3.Normalize(point - prevPoint);

            if (vertices.Count == 0)
            {
                CreateVertices(prevPoint, width, forward);
            }

            int segments = Convert.ToInt32(dir.magnitude / segmentLength) + 1;
            for (int iSeg = 0; iSeg < segments; iSeg++)
            {
                float tSeg = (iSeg + 1) / (float)segments;
                currTexV += tSeg * dir.magnitude / segmentLength;

                Vector3 pos = Vector3.Lerp(prevPoint, point, tSeg);

                CreateVertices(pos, width, forward);

                int iLastVert = vertices.Count - 1;

                indices.Add(iLastVert - 3);
                indices.Add(iLastVert - 2);
                indices.Add(iLastVert - 1);
                indices.Add(iLastVert - 2);
                indices.Add(iLastVert - 0);
                indices.Add(iLastVert - 1);
            }
        }

        prevPoint = point;
    }

    private void CreateVertices(Vector3 point, float width, Vector3 forward)
    {
        Vector3 right = Vector3.Cross(forward, Vector3.up);
        Vector3 up = Vector3.Cross(right, forward);
        vertices.Add(point - right * width * 0.5f);
        vertices.Add(point + right * width * 0.5f);
        normals.Add(up);
        normals.Add(up);
        uv.Add(new Vector2(0.0f, currTexV));
        uv.Add(new Vector2(1.0f, currTexV));
    }
}
