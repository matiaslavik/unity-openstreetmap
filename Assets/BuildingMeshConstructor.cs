﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BuildingMeshConstructor
{
    public Mesh ConstructWalls(List<Vector2> nodes, float height)
    {
        List<Vector3> verts = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<int> indices = new List<int>();

        for (int iNode = 1; iNode < nodes.Count; iNode++)
        {
            Vector2 aNode = nodes[iNode - 1];
            Vector2 bNode = nodes[iNode];

            verts.Add(new Vector3(aNode.x, 0.0f, aNode.y));
            verts.Add(new Vector3(bNode.x, 0.0f, bNode.y));
            verts.Add(new Vector3(bNode.x, height, bNode.y));
            verts.Add(new Vector3(aNode.x, height, aNode.y));

            Vector3 normal = Vector3.Cross(aNode, bNode).normalized;
            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);

            int lastVertIndex = verts.Count - 1;

            indices.Add(lastVertIndex - 1);
            indices.Add(lastVertIndex - 2);
            indices.Add(lastVertIndex - 0);
            indices.Add(lastVertIndex - 0);
            indices.Add(lastVertIndex - 2);
            indices.Add(lastVertIndex - 3);
        }

        Mesh mesh = new Mesh();
        mesh.vertices = verts.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = indices.ToArray();
        return mesh;
    }

    public Mesh ConstructRoof(List<Vector2> nodes, float height)
    {
        List<Vector3> verts = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        int[] indices = null;

        // Add vertices
        for (int iNode = 0; iNode < nodes.Count; iNode++)
        {
            Vector2 node = nodes[iNode];
            verts.Add(new Vector3(node.x, height, node.y));
            normals.Add(Vector3.up);
        }

        nodes.RemoveAt(nodes.Count - 1); // In OSM: the last node is the same as the first
        Triangulator triangulator = new Triangulator(nodes);
        indices = triangulator.Triangulate();

        if (indices.Length < 3)
            return null;

        Mesh mesh = new Mesh();
        mesh.vertices = verts.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = indices;
        return mesh;
    }
}
