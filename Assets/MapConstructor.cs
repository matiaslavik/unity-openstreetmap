﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MapConstructor
{
    const int EARTH_RADIUS = 6378137;

    private MapAreaData mapData;
    private HashSet<long> processedRoads = new HashSet<long>();

    double centreLat;
    double centreLon;

    public void ConstructMap(MapAreaData mapData)
    {
        this.mapData = mapData;

        centreLat = mapData.nodes.Values.First().lat;
        centreLon = mapData.nodes.Values.First().lon;

        foreach (WayInfo way in mapData.ways.Values)
        {
            ConstructWay(way);
        }
    }

    // https://wiki.openstreetmap.org/wiki/Mercator
    private double lat2z_m(double lat)
    {
        return Math.Log(Math.Tan(((double)Mathf.Deg2Rad) * lat / 2.0f + Math.PI / 4.0f)) * (double)EARTH_RADIUS;
    }

    // https://wiki.openstreetmap.org/wiki/Mercator
    private double lon2x_m(double lon)
    {
        return ((double)Mathf.Deg2Rad) * lon * (double)EARTH_RADIUS;
    }

    private void ConstructWay(WayInfo way)
    {
        if (way.roadInfo != null)
            ConstructRoad(way);
        else if (way.buildingInfo != null)
            ConstructBuilding(way);
    }

    private void ConstructRoad(WayInfo way)
    {
        if (processedRoads.Contains(way.id))
            return;

        processedRoads.Add(way.id);

        float roadWidth = 5.0f;

        RoadMeshConstructor meshConstructor = new RoadMeshConstructor();

        foreach (WayNodeInfo wayNode in way.wayNodes)
        {
            double dLat = wayNode.node.lat - centreLat;
            double dLon = wayNode.node.lon - centreLon;
            double x = lon2x_m(dLon);
            double z = lat2z_m(dLat);

            //GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //obj.transform.position = new Vector3((float)x, 0.0f, (float)z);

            meshConstructor.AddPoint(new Vector3((float)x, 0.0f, (float)z), roadWidth);

            foreach (WayInfo connectedWay in wayNode.connectedWays)
            {
                ConstructWay(connectedWay);
            }
        }

        Mesh roadMesh = meshConstructor.CreateMesh();
        GameObject roadObj = new GameObject();
        roadObj.transform.position = Vector3.zero;
        MeshFilter meshFilter = roadObj.AddComponent<MeshFilter>();
        meshFilter.mesh = roadMesh;
        MeshRenderer meshRenderer = roadObj.AddComponent<MeshRenderer>();
        meshRenderer.material = MapResourceData.instance.roadMaterial;
    }

    private void ConstructBuilding(WayInfo way)
    {
        List<Vector2> nodePositions = new List<Vector2>();

        foreach (WayNodeInfo wayNode in way.wayNodes)
        {
            double dLat = wayNode.node.lat - centreLat;
            double dLon = wayNode.node.lon - centreLon;
            double x = lon2x_m(dLon);
            double z = lat2z_m(dLat);

            Vector2 nodePos = new Vector2((float)x, (float)z);
            nodePositions.Add(nodePos);
        }

        if (nodePositions.Count < 3)
            return;

        // Fix winding order
        float windingSum = 0.0f;
        for(int i = 0; i < nodePositions.Count; i++)
        {
            Vector2 a = nodePositions[i];
            Vector2 b = nodePositions[(i + 1) % nodePositions.Count];

            windingSum += (b.x - a.x) * (b.y + a.y);
        }
        if (windingSum > 0)
            nodePositions.Reverse();

        BuildingMeshConstructor buildingConstructor = new BuildingMeshConstructor();

        // Construct walls
        Mesh wallMesh = buildingConstructor.ConstructWalls(nodePositions, 10.0f);
        if(wallMesh != null)
        {
            GameObject wallObj = new GameObject("wall");
            wallObj.transform.position = Vector3.zero;
            MeshFilter wallMeshFilter = wallObj.AddComponent<MeshFilter>();
            wallMeshFilter.mesh = wallMesh;
            MeshRenderer wallMeshRenderer = wallObj.AddComponent<MeshRenderer>();
            wallMeshRenderer.material = MapResourceData.instance.buildingMaterial;
        }

        // Construct roof
        Mesh roofMesh = buildingConstructor.ConstructRoof(nodePositions, 10.0f);
        if(roofMesh != null)
        {
            GameObject roofObj = new GameObject("roof");
            roofObj.transform.position = Vector3.zero;
            MeshFilter roofMeshFilter = roofObj.AddComponent<MeshFilter>();
            roofMeshFilter.mesh = roofMesh;
            MeshRenderer roofMeshRenderer = roofObj.AddComponent<MeshRenderer>();
            roofMeshRenderer.material = MapResourceData.instance.buildingMaterial;
        }
    }
}
